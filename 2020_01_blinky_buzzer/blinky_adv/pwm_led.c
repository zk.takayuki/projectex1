/*
 * pwm_led.c
 *
 *  Created on: 2021/10/31
 *      Author: Amalgam
 */
#include "pwm_led.h"

void initPWM() {
    assignPinsGPIO();
    // For led PIN1-3 initialize
    PWMGenConfigure(PWM1_BASE, PWM_GEN_2, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);
    PWMGenConfigure(PWM1_BASE, PWM_GEN_3, PWM_GEN_MODE_UP_DOWN | PWM_GEN_MODE_NO_SYNC);

    // Set PWM Period
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_2, 10000);
    PWMGenPeriodSet(PWM1_BASE, PWM_GEN_3, 10000);

    // PulseWidth set
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_5, 10000);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_6, 10000);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_7, 10000);

    // Permit to outputPWM
    PWMOutputState(PWM1_BASE, PWM_OUT_5_BIT, true);
    PWMOutputState(PWM1_BASE, PWM_OUT_6_BIT, true);
    PWMOutputState(PWM1_BASE, PWM_OUT_7_BIT, true);

    // Enable PWM Gen
    PWMGenEnable(PWM1_BASE, PWN_GEN_2);
    PWMGenEnable(PWM1_BASE, PWN_GEN_3);
}

void assignPinsGPIO() {
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_PWM1);

    // GPIO Pins assign to PWM see table2.2-2.4
    GPIOPinConfigure(GPIO_PF2_M1PWM6);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_2);

    GPIOPinConfigure(GPIO_PF3_M1PWM7);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_3);

    GPIOPinConfigure(GPIO_PF1_M1PWM5);
    GPIOPinTypePWM(GPIO_PORTF_BASE, GPIO_PIN_1);
}

void setPulseWidth(uint32_t width) {
    if (width > 10000) {
        width = 10000;
    } else if (width < 0) {
        width = 0;
    }
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_5, width);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_6, width);
    PWMPulseWidthSet(PWM1_BASE, PWM_OUT_7, width);
}

void startPWM() {
    PWMOutputState(PWM1_BASE, PWM_OUT_5_BIT, true);
    PWMOutputState(PWM1_BASE, PWM_OUT_6_BIT, true);
    PWMOutputState(PWM1_BASE, PWM_OUT_7_BIT, true);
}

void stopPWM() {
    PWMOutputState(PWM1_BASE, PWM_OUT_5_BIT, false);
    PWMOutputState(PWM1_BASE, PWM_OUT_6_BIT, false);
    PWMOutputState(PWM1_BASE, PWM_OUT_7_BIT, false);
}
